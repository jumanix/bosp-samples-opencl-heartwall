
cmake_minimum_required(VERSION 3.0)

set (BOSP_SYSROOT ${CMAKE_INSTALL_PREFIX})
find_package(BbqRTLib REQUIRED)

#----- Add compilation dependencies
include_directories(${BBQUE_RTLIB_INCLUDE_DIR},
	util/avi
	util/timer
	util/file)

#----- Add "heartwallocl" target application
set(HEARTWALLOCL_SRC version HeartWallOCL_exc HeartWallOCL_main
	util/avi/avilib
	util/avi/avimod
	util/file/file
	util/timer/timer
	util/opencl/opencl
)

add_executable(heartwallocl ${HEARTWALLOCL_SRC})

#----- Linking dependencies
target_link_libraries(
	heartwallocl
	${Boost_LIBRARIES}
	${BBQUE_RTLIB_LIBRARY}
)

# Link against the vendor OpenCL library
if (NOT CONFIG_BBQUE_OPENCL)
  find_package (OpenCL REQUIRED)
  target_link_libraries(
	heartwallocl
	${OPENCL_LIBRARIES}
  )
endif (NOT CONFIG_BBQUE_OPENCL)

include_directories (${OPENCL_INCLUDE_DIR})

# Use link path ad RPATH
set_property(TARGET heartwallocl PROPERTY
	INSTALL_RPATH_USE_LINK_PATH TRUE)

#----- Install the HeartWallOCL files
install (TARGETS heartwallocl RUNTIME
	DESTINATION ${HEARTWALLOCL_PATH_BINS})

#----- Generate and Install HeartWallOCL configuration file
configure_file (
	"${PROJECT_SOURCE_DIR}/HeartWallOCL.conf.in"
	"${PROJECT_BINARY_DIR}/HeartWallOCL.conf"
)
install (FILES "${PROJECT_BINARY_DIR}/HeartWallOCL.conf"
	DESTINATION ${HEARTWALLOCL_PATH_CONFIG})
