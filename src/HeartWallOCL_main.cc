/**
 *       @file  HeartWallOCL_main.cc
 *      @brief  The HeartWallOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "HeartWallOCL_exc.h"
#include <bbque/config.h>
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

// Application specific inclusion
#include <cstdlib>
#include <cmath>
#include "util/timer/timer.h"
#include "util/file/file.h"

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "HeartWallOCL"

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;

/**
 * The decription of each HeartWallOCL parameters
 */
po::options_description opts_desc("HeartWallOCL Configuration Options");

/**
 * The map of all HeartWallOCL parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/HeartWallOCL.conf" ;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;

/**
 * @brief The EXecution Context (EXC) registered
 */
pBbqueEXC_t pexc;

void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "HeartWallOCL (ver. " << g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]) {

	// time
	long long time0;
	long long time1;
	long long time2;
	long long time3;
	long long time4;
	long long time5;

	// other
	int awmid = -1;
	int wgsize = 0;
	avi_t* frames;

	opts_desc.add_options()
		("help,h", "print this help message")
		("version,v", "print program version")

		("conf,C", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"HeartWallOCL configuration file")

		("recipe,r", po::value<std::string>(&recipe)->
			default_value("HeartWallOCL"),
			"recipe name (for all EXCs)")
		("wgsize,s", po::value<int>(&wgsize)->
			default_value(256),
			"Workgroup size")
		("awm,w", po::value<int>(&awmid)->
			default_value(-1),
			"AWM id to force")
	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("heartwallocl");

	ParseCommandLine(argc, argv);

	// Welcome screen
	logger->Info(".:: HeartWallOCL (ver. %s) ::.", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__);
	time0 = get_time();

	//======================================================================
	//	STRUCTURES, GLOBAL STRUCTURE VARIABLES
	//======================================================================
	params_common common;
	common.common_mem = sizeof(params_common);

	//======================================================================
	// 	FRAME INFO
	//======================================================================
	// open movie file
	char * video_file_name = { BBQUE_PATH_PREFIX "/" PATH_DATA "/heartwall/test.avi"};
	logger->Info("Opening %s...", video_file_name);
	frames = (avi_t*)AVI_open_input_file(video_file_name, 1);
	if (frames == NULL)  {
		AVI_print_error((char *) "Error with AVI_open_input_file");
		return -1;
	}

	// dimensions
	common.no_frames = AVI_video_frames(frames);
	common.frame_rows = AVI_video_height(frames);
	common.frame_cols = AVI_video_width(frames);
	common.frame_elem = common.frame_rows * common.frame_cols;
	common.frame_mem = sizeof(fp) * common.frame_elem;

	time1 = get_time();

	//======================================================================
	// 	CHECK INPUT ARGUMENTS
	//======================================================================

	if(argc < 2){
		printf("ERROR: missing argument (number of frames to processed) "
			"or too many arguments\n");
		return 0;
	}
	else{
		common.frames_processed = atoi(argv[1]);
		if(common.frames_processed<0 || common.frames_processed>common.no_frames){
			printf("ERROR: %d is an incorrect number of frames specified,"
				" select in the range of 0-%d\n",
				common.frames_processed, common.no_frames);
			return 0;
		}
	}

	time2 = get_time();

	//======================================================================
	//	INPUTS
	//======================================================================
	char dpath[] = { BBQUE_PATH_PREFIX "/" PATH_DATA "/heartwall/input.txt" };

	//======================================================================
	//	READ PARAMETERS FROM FILE
	//======================================================================
	read_parameters(dpath,
			&common.tSize,
			&common.sSize,
			&common.maxMove,
			&common.alpha);

	//======================================================================
	//	READ SIZE OF INPUTS FROM FILE
	//======================================================================
	read_header(dpath, &common.endoPoints, &common.epiPoints);
	common.allPoints = common.endoPoints + common.epiPoints;

	//======================================================================
	//	READ DATA FROM FILE
	//======================================================================

	//======================================================================
	//	ENDO POINTS MEMORY ALLOCATION
	//======================================================================
	common.endo_mem = sizeof(int) * common.endoPoints;

	int* endoRow;
	endoRow = (int*)malloc(common.endo_mem);
	int* endoCol;
	endoCol = (int*)malloc(common.endo_mem);
	int* tEndoRowLoc;
	tEndoRowLoc = (int*)malloc(common.endo_mem * common.no_frames);
	int* tEndoColLoc;
	tEndoColLoc = (int*)malloc(common.endo_mem * common.no_frames);

	//======================================================================
	//	EPI POINTS MEMORY ALLOCATION
	//======================================================================
	common.epi_mem = sizeof(int) * common.epiPoints;

	int* epiRow;
	epiRow = (int *)malloc(common.epi_mem);
	int* epiCol;
	epiCol = (int *)malloc(common.epi_mem);
	int* tEpiRowLoc;
	tEpiRowLoc = (int *)malloc(common.epi_mem * common.no_frames);
	int* tEpiColLoc;
	tEpiColLoc = (int *)malloc(common.epi_mem * common.no_frames);

	//======================================================================
	//	READ DATA FROM FILE
	//======================================================================
	read_data(
		dpath,
		common.endoPoints,
		endoRow,
		endoCol,
		common.epiPoints,
		epiRow,
		epiCol);
	time3 = get_time();

	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib, application [%s]...",
			::basename(argv[0]));
	RTLIB_Init(::basename(argv[0]), &rtlib);
	assert(rtlib);

	logger->Info("STEP 1. Registering EXC using [%s] recipe...",
			recipe.c_str());
	pexc = pBbqueEXC_t(new HeartWallOCL(
			"HeartWallOCL", recipe, rtlib,
			common,
			endoRow,
			endoCol,
			tEndoRowLoc,
			tEndoColLoc,
			epiRow,
			epiCol,
			tEpiRowLoc,
			tEpiColLoc,
			frames,
			wgsize,
			awmid));
	if (!pexc->isRegistered())
		return RTLIB_ERROR;


	logger->Info("STEP 2. Starting EXC control thread...");
	pexc->Start();

	logger->Info("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();

	time4 = get_time();

	//==================================================50
	//	DUMP DATA TO FILE
	//==================================================50
	write_data(
		"result.txt",
		common.no_frames,
		common.frames_processed,
		common.endoPoints,
		tEndoRowLoc,
		tEndoColLoc,
		common.epiPoints,
		tEpiRowLoc,
		tEpiColLoc);

	//==================================================50
	//	End
	//==================================================50
	free(endoRow);
	free(endoCol);
	free(tEndoRowLoc);
	free(tEndoColLoc);

	free(epiRow);
	free(epiCol);
	free(tEpiRowLoc);
	free(tEpiColLoc);

	time5= get_time();

	//======================================================================
	//	DISPLAY TIMING
	//======================================================================

	printf("Time spent in different stages of the application:\n");
	printf("%15.12f s, %15.12f % : READ INITIAL VIDEO FRAME\n",
		(fp) (time1-time0) / 1000000, (fp) (time1-time0) / (fp) (time5-time0) * 100);
	printf("%15.12f s, %15.12f % : READ COMMAND LINE PARAMETERS\n",
		(fp) (time2-time1) / 1000000, (fp) (time2-time1) / (fp) (time5-time0) * 100);
	printf("%15.12f s, %15.12f % : READ INPUTS FROM FILE\n",
		(fp) (time3-time2) / 1000000, (fp) (time3-time2) / (fp) (time5-time0) * 100);
	printf("%15.12f s, %15.12f % : GPU ALLOCATION, COPYING, COMPUTATION\n",
		(fp) (time4-time3) / 1000000, (fp) (time4-time3) / (fp) (time5-time0) * 100);
	printf("%15.12f s, %15.12f % : FREE MEMORY\n",
		(fp) (time5-time4) / 1000000, (fp) (time5-time4) / (fp) (time5-time0) * 100);
	printf("Total time:\n");
	printf("%15.12f s\n", (fp) (time5-time0) / 1000000);

	logger->Info("STEP 4. Disabling EXC...");
	pexc = NULL;

	logger->Info("===== HeartWallOCL DONE! =====");
	return EXIT_SUCCESS;
}

