/**
 *       @file  HeartWallOCL_exc.h
 *      @brief  The HeartWallOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef HEARTWALLOCL_EXC_H_
#define HEARTWALLOCL_EXC_H_

#include <bbque/bbque_exc.h>
#ifndef __OPENCL_CL_H
#include <CL/cl.h>
#endif //__OPENCL_CL_H

#include "../src/main.h"
#include "../src/util/avi/avilib.h"
#include "../src/util/avi/avimod.h"

// The highest id number for AWM featuring only the CPU
#define AWM_ID_MAX_CPU 7

using bbque::rtlib::BbqueEXC;

class HeartWallOCL : public BbqueEXC {

public:

	HeartWallOCL(
		std::string const & name, std::string const & recipe, RTLIB_Services_t *rtlib,
		params_common common,
		int* endoRow,
		int* endoCol,
		int* tEndoRowLoc,
		int* tEndoColLoc,
		int* epiRow,
		int* epiCol,
		int* tEpiRowLoc,
		int* tEpiColLoc,
		avi_t* _frames,
		int _wgSize,
		int _awmId);

private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();

	void Cleanup();

	// Application-specific
	params_common common;
	int* endoRow;
	int* endoCol;
	int* tEndoRowLoc;
	int* tEndoColLoc;
	int* epiRow;
	int* epiCol;
	int* tEpiRowLoc;
	int* tEpiColLoc;
	avi_t* frames;
	int wgSize;
	int awmId;

	int error;
	cl_platform_id *platforms;
	cl_device_id* devices;
	cl_context context;
	cl_command_queue command_queue;
	cl_program program;
	cl_kernel kernel;

	cl_mem d_common;
	cl_mem d_frame;
	cl_mem d_endoRow;
	cl_mem d_endoCol;
	cl_mem d_tEndoRowLoc;
	cl_mem d_tEndoColLoc;
	cl_mem d_epiRow;
	cl_mem d_epiCol;
	cl_mem d_tEpiColLoc;
	cl_mem d_tEpiRowLoc;
	cl_mem d_endoT;
	cl_mem d_epiT;
	cl_mem d_in2;
	cl_mem d_conv;
	cl_mem d_in2_pad_cumv;
	cl_mem d_in2_pad_cumv_sel;
	cl_mem d_in2_sub_cumh;
	cl_mem d_in2_sub_cumh_sel;
	cl_mem d_in2_sub2;
	cl_mem d_in2_sqr;
	cl_mem d_in2_sqr_sub2;
	cl_mem d_in_sqr;
	cl_mem d_tMask;
	cl_mem d_mask_conv;
	cl_mem d_checksum;

	size_t local_work_size[1];
	size_t global_work_size[1];
};

#endif // HEARTWALLOCL_EXC_H_

