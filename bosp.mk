
ifdef CONFIG_SAMPLES_OPENCL_HEARTWALL

# Targets provided by this project
.PHONY: samples_opencl_heartwall clean_samples_opencl_heartwall

# Add this to the "contrib_samples_opencl" target
samples_opencl: samples_opencl_heartwall
clean_samples_opencl: clean_samples_opencl_heartwall

MODULE_SAMPLES_OPENCL_HEARTWALLOCL=samples/opencl/heartwall

samples_opencl_heartwall: 
	@echo
	@echo "==== Building HeartWallOCL ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_OPENCL_HEARTWALLOCL)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_OPENCL_HEARTWALLOCL)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCL_HEARTWALLOCL)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCL_HEARTWALLOCL)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_opencl_heartwall:
	@echo
	@echo "==== Clean-up HeartWallOCL Application ===="
	@[ ! -f $(BOSP_SYSROOT)/usr/bin/heartwallocl ] || \
		rm -f $(BOSP_SYSROOT)/etc/bbque/recipes/HeartWallOCL*; \
		rm -rf $(BOSP_SYSROOT)/usr/bin/heartwallocl*
	@rm -rf $(MODULE_SAMPLES_OPENCL_HEARTWALLOCL)/build
	@echo

run-ocl-heartwall: samples_opencl_heartwall
	@echo === Running HeartWall OCL ===
	${BOSP_SYSROOT}/usr/bin/heartwallocl 10 256 11

else # CONFIG_SAMPLES_HEARTWALLOCL

samples_opencl_heartwall:
	$(warning HeartWallOCL disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_OPENCL_HEARTWALL
